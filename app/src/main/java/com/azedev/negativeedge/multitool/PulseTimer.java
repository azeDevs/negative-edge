package com.azedev.negativeedge.multitool;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public final class PulseTimer extends TimerTask {

    public static final long PPS60 = 16;
    private List<SyncedTask> queue = new ArrayList<>();
    private long currentPulse = 0;

    public PulseTimer(long intervalMilliseconds) {
        new Timer().schedule(this, intervalMilliseconds, intervalMilliseconds);
    }

    public void queue(Runnable runnable, long delay) { queue.add(new SyncedTask(runnable, delay)); }

    @Override public void run() {
        currentPulse++;
        queue.stream().filter(syncedTask -> syncedTask.runAndRemove());
    }

    private class SyncedTask {
        private long pulseDelay;
        private long queuedPulse;
        private Runnable runnable;
        public SyncedTask(Runnable runnable, long delay) {
            this.queuedPulse = currentPulse;
            this.pulseDelay = delay; }
        public boolean runAndRemove() {
            if (currentPulse >= queuedPulse + pulseDelay) {
                runnable.run();
                return false;
            } else return true;
        }
    }

}