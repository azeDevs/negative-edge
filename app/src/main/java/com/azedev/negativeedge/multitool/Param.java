package com.azedev.negativeedge.multitool;


public final class Param {

    private String value;

    public Param() { this.value = "0"; }
    public Param(String value) { this.value = value; }
    public Param(Boolean value) { this.value = value.toString(); }
    public Param(Integer value) { this.value = value.toString(); }
    public Param(Long value) { this.value = value.toString(); }

    private void logParseFailure() {
        System.out.println(String.format("Failed to parse Param: %s", value));
    }


    // String

    @Override public String toString() {
        return value;
    }

    public boolean filtersTo(String regex, String comparison) {
        return value.replaceAll(regex, "").equalsIgnoreCase(comparison);
    }


    // Boolean

    public Boolean toBool(boolean defaultTo) {
        if (toBool() != null) return toBool();
        else return defaultTo;
    }

    public Boolean toBool() {
        Boolean result = null;
        try { result = Boolean.valueOf(value.replaceAll("[^\\d]", ""));
        } catch (NumberFormatException e) { logParseFailure(); }
        return result;
    }

    public boolean isBool() {
        return toBool() != null;
    }


    // Integer

    public Integer toInt(int min, int max) {
        if (isInt()) return Math.min(Math.max(toInt(), min), max);
        else return null;
    }

    public Integer toInt(int defaultTo) {
        if (toInt() != null) return toInt();
        else return defaultTo;
    }

    public Integer toInt() {
        Integer result = null;
        try { result = Integer.valueOf(value.replaceAll("[^\\d]", ""));
        } catch (NumberFormatException e) { logParseFailure(); }
        return result;
    }

    public boolean isInt() {
        return toInt() != null;
    }


    // Long

    public Long toLong() {
        Long result = null;
        try { result = Long.valueOf(value.replaceAll("[^\\d]", ""));
        } catch (NumberFormatException e) { logParseFailure(); }
        return result;
    }

    public Long toLong(long defaultTo) {
        if (toLong() != null) return toLong();
        else return defaultTo;
    }

    public boolean isLong() {
        return toLong() != null;
    }


    // Double

    public Double toDouble() {
        Double result = null;
        try { result = Double.valueOf(value.replaceAll("[^\\d]", ""));
        } catch (NumberFormatException e) { logParseFailure(); }
        return result;
    }

}
