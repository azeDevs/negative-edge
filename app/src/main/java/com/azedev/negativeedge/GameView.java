package com.azedev.negativeedge;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.azedev.negativeedge.renderable.GraphDebug;


public class GameView extends View {

    GraphDebug debug;
    Paints paints;
    long frames = 0;
    boolean p1, p2;

    private void init(Context context) {
        this.debug = new GraphDebug();
        this.paints = new Paints(context);
    }

    @Override public boolean onTouchEvent(MotionEvent event) {
        int pointerIndex = event.getActionIndex();
        int pointerId = event.getPointerId(pointerIndex);
        int maskedAction = event.getActionMasked();

        debug.update("ACTION_DOWN", false);
        debug.update("ACTION_UP", false);
        debug.update("ACTION_POINTER_DOWN", false);
        debug.update("ACTION_POINTER_UP", false);

        switch (maskedAction) {
            case MotionEvent.ACTION_DOWN:
                debug.update("ACTION_DOWN", true);
                if (event.getY() > getHeight()/2) p1 = true;
                else p2 = true;
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                debug.update("ACTION_UP", true);
                if (event.getY() > getHeight()/2) p1 = false;
                else p2 = false;
                invalidate();
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                debug.update("ACTION_POINTER_DOWN", true);
                if (event.getY(1) > getHeight()/2) p1 = true;
                else p2 = true;
                invalidate();
                break;
            case MotionEvent.ACTION_POINTER_UP:
                debug.update("ACTION_POINTER_UP", true);
                if (event.getY(1) > getHeight()/2) p1 = false;
                else p2 = false;
                invalidate();
                break;
//            case MotionEvent.ACTION_CANCEL:
//                if (event.getY() > getHeight()/2) p1 = false;
//                else p2 = false;
//                invalidate();
//                break;
        }
        return super.onTouchEvent(event);
    }

    public GameView(Context context) { super(context); init(context); }
    public GameView(Context context, @Nullable AttributeSet attrs) { super(context, attrs); init(context); }
    public GameView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) { super(context, attrs, defStyleAttr); init(context); }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        frames++;
        final float btnScale = 160.0f;
        final int btnPosWidth = getWidth() / 2;
        final int btnPosHeight = getHeight() / 8;
        paintTheButtons(canvas, btnScale, btnPosWidth, btnPosHeight);
        debug.update("frames", frames);
        debug.update("player1", p1);
        debug.update("player2", p2);
        debug.paint(canvas);
    }

    private void paintTheButtons(Canvas canvas, float btnScale, int btnPosWidth, int btnPosHeight) {
        if (p1) hex(canvas, new Point(btnPosWidth, getHeight()-btnPosHeight), btnScale, paints.grn[2]);
        else hex(canvas, new Point(btnPosWidth, getHeight()-btnPosHeight), btnScale, paints.blu[1]);
        if (p2) hex(canvas, new Point(btnPosWidth, btnPosHeight), btnScale, paints.red[2]);
        else hex(canvas, new Point(btnPosWidth, btnPosHeight), btnScale, paints.blu[1]);
    }

    public void hex(Canvas canvas, Point center, float radius, Paint hexPaint) {
        Point[] points = new Point[6];
        for (int i=0; i<6; i++) {
            float angle_deg = 60 * i + 30;
            float angle_rad = (float) Math.PI / 180 * angle_deg;
            points[i] = new Point();
            points[i].x = (int) (center.x + radius * (float) Math.cos(angle_rad));
            points[i].y = (int) (center.y + radius * (float) Math.sin(angle_rad));
        }
        canvas.drawPath(getPath(points), hexPaint);
    }

    private Path getPath(Point[] point) {
        Path path = new Path();
        path.reset();
        path.moveTo(point[0].x, point[0].y);
        for (int i = 1; i < point.length; i++) path.lineTo(point[i].x, point[i].y);
        path.lineTo(point[0].x, point[0].y);
        return path;
    }

}
