package com.azedev.negativeedge;

import android.content.Context;
import android.graphics.Paint;


public class Paints {

    private Context context;
    public static Paint[] bkg, blu, pur, red, grn;

    public Paints(Context context) {
        this.context = context;
        bkg = getPaints(R.color.bkg0, R.color.bkg1, R.color.bkg2, R.color.bkg3);
        blu = getPaints(R.color.blu0, R.color.blu1, R.color.blu2, R.color.blu3);
        pur = getPaints(R.color.pur0, R.color.pur1, R.color.pur2, R.color.pur3);
        red = getPaints(R.color.red0, R.color.red1, R.color.red2, R.color.red3);
        grn = getPaints(R.color.grn0, R.color.grn1, R.color.grn2, R.color.grn3);
    }

    private Paint[] getPaints(int... shades) {
        Paint[] paints = new Paint[shades.length];
        for (int n=0; n<paints.length; n++) {
            paints[n] = new Paint();
            paints[n].setTextSize(40);
            paints[n].setAntiAlias(true);
            paints[n].setStyle(Paint.Style.FILL);
            paints[n].setColor(context.getResources().getColor(shades[n]));
        }
        return paints;
    }

}
