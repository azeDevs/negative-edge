package com.azedev.negativeedge.renderable;
import android.graphics.Canvas;


abstract class CanvasGraph {

    int posX;
    int posY;

    public CanvasGraph(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public abstract void paint(Canvas canvas);

}
