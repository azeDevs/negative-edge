package com.azedev.negativeedge.renderable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.azedev.negativeedge.R;


public class PixelView extends View {

    private Paint paintFace = new Paint();
    private Bitmap bitmapBuffer;
    private BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();

    public PixelView(Context context) {
        super(context);
        init();
    }
    public PixelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public PixelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.bitmapFactoryOptions.inScaled = false;
        this.bitmapBuffer = BitmapFactory.decodeResource(this.getResources(), R.drawable.pixel_dog, bitmapFactoryOptions);

        int colorFace = Color.parseColor("#6c9b45");
        paintFace.setARGB(255, Color.red(colorFace), Color.green(colorFace), Color.blue(colorFace));
        paintFace.setFilterBitmap(false);
        paintFace.setDither(false);
        paintFace.setAntiAlias(false);
        paintFace.setStyle(Paint.Style.STROKE);
        paintFace.setStrokeWidth(1);
    }

    @Override protected void onDraw(Canvas canvas) {
        Rect rectSource = new Rect(4, 4, 60, 60);
        Rect rectCanvas = new Rect(128, 128, 512, 512);
        canvas.drawRect(rectCanvas, paintFace);
        canvas.drawBitmap(bitmapBuffer, rectSource, rectCanvas, paintFace);
        super.onDraw(canvas);
    }

}
