package com.azedev.negativeedge.renderable;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;

import com.azedev.negativeedge.Paints;
import com.azedev.negativeedge.multitool.Param;

import java.util.HashMap;

import static android.text.Layout.Alignment.ALIGN_NORMAL;


public class GraphDebug extends CanvasGraph {

    private HashMap<String, HashMap<String, Param>> debugMap;

    public GraphDebug() {
        super(40, 40);
        debugMap = new HashMap<>();
    }

    public void update(String label, int value) { update(label, new Param(value)); }
    public void update(String label, long value) { update(label, new Param(value)); }
    public void update(String label, boolean value) { update(label, new Param(value)); }

    public void update(String labelKey, Param value) {
        String groupKey = Thread.currentThread().getStackTrace()[4].getMethodName();
        if (!debugMap.containsKey(groupKey)) debugMap.put(groupKey, new HashMap<>());
        debugMap.get(groupKey).put(labelKey, value);
    }

    @Override public void paint(Canvas canvas) {
        final int width = canvas.getWidth();
        final TextPaint paint = new TextPaint(Paints.blu[3]);
        StringBuilder sb = new StringBuilder("GAMEVIEW DEBUG");
        debugMap.entrySet().stream().forEach(group -> {
            sb.append("\n\n[ " + group.getKey() + " ]\n");
            group.getValue().entrySet().stream().forEach(entry ->
                    sb.append(String.format("\n%s = %s", entry.getKey(), entry.getValue()))); });
        StaticLayout textLayout = new StaticLayout(sb.toString(), paint, width, ALIGN_NORMAL, 1.0f, 0.0f, false);
        canvas.save();
        canvas.translate(40, 40);
        textLayout.draw(canvas);
        canvas.restore();
    }

}
