package com.azedev.negativeedge;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.azedev.negativeedge.multitool.PulseTimer;

import static com.azedev.negativeedge.multitool.PulseTimer.PPS60;


public class GameActivity extends AppCompatActivity {

    private GameView gameView;
    private PulseTimer timeKeeper;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_fullscreen);
        this.gameView = findViewById(R.id.game);
        this.timeKeeper = new PulseTimer(PPS60);
    }

}
